﻿namespace GenericThermalAnalyzer.Gui
{
	partial class TransitionControl
	{
		/// <summary> 
		/// Wymagana zmienna projektanta.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Wyczyść wszystkie używane zasoby.
		/// </summary>
		/// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Kod wygenerowany przez Projektanta składników

		/// <summary> 
		/// Metoda wymagana do obsługi projektanta — nie należy modyfikować 
		/// jej zawartości w edytorze kodu.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.FlowLayoutPanel tempIntervalKelvinFlowPanel;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
            System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
            System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
            this.initialTempUpDown = new System.Windows.Forms.NumericUpDown();
            this.bindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.finalTempUpDown = new System.Windows.Forms.NumericUpDown();
            this.configurationTablePanel = new System.Windows.Forms.TableLayoutPanel();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.distributionFunctionComboBox = new System.Windows.Forms.ComboBox();
            this.totalEnthalpyUpDown = new System.Windows.Forms.NumericUpDown();
            this.deleteButton = new System.Windows.Forms.Button();
            this.mainGroupBox = new System.Windows.Forms.GroupBox();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            tempIntervalKelvinFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            label4 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            tempIntervalKelvinFlowPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.initialTempUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.finalTempUpDown)).BeginInit();
            tableLayoutPanel3.SuspendLayout();
            this.configurationTablePanel.SuspendLayout();
            flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.totalEnthalpyUpDown)).BeginInit();
            flowLayoutPanel1.SuspendLayout();
            this.mainGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // tempIntervalKelvinFlowPanel
            // 
            tempIntervalKelvinFlowPanel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            tempIntervalKelvinFlowPanel.AutoSize = true;
            tempIntervalKelvinFlowPanel.Controls.Add(this.initialTempUpDown);
            tempIntervalKelvinFlowPanel.Controls.Add(label4);
            tempIntervalKelvinFlowPanel.Controls.Add(this.finalTempUpDown);
            tempIntervalKelvinFlowPanel.Location = new System.Drawing.Point(117, 61);
            tempIntervalKelvinFlowPanel.Name = "tempIntervalKelvinFlowPanel";
            tempIntervalKelvinFlowPanel.Size = new System.Drawing.Size(143, 26);
            tempIntervalKelvinFlowPanel.TabIndex = 0;
            // 
            // initialTempUpDown
            // 
            this.initialTempUpDown.AutoSize = true;
            this.initialTempUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bindingSource, "InitialTemperature", true));
            this.initialTempUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.initialTempUpDown.Location = new System.Drawing.Point(3, 3);
            this.initialTempUpDown.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.initialTempUpDown.Minimum = new decimal(new int[] {
            273,
            0,
            0,
            -2147483648});
            this.initialTempUpDown.Name = "initialTempUpDown";
            this.initialTempUpDown.Size = new System.Drawing.Size(56, 20);
            this.initialTempUpDown.TabIndex = 3;
            this.initialTempUpDown.ThousandsSeparator = true;
            this.initialTempUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // bindingSource
            // 
            this.bindingSource.DataSource = typeof(GenericThermalAnalyzer.Transition.Abstract.ITransition);
            // 
            // label4
            // 
            label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(65, 6);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(13, 13);
            label4.TabIndex = 5;
            label4.Text = "—";
            // 
            // finalTempUpDown
            // 
            this.finalTempUpDown.AutoSize = true;
            this.finalTempUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bindingSource, "FinalTemperature", true));
            this.finalTempUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.finalTempUpDown.Location = new System.Drawing.Point(84, 3);
            this.finalTempUpDown.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.finalTempUpDown.Minimum = new decimal(new int[] {
            273,
            0,
            0,
            -2147483648});
            this.finalTempUpDown.Name = "finalTempUpDown";
            this.finalTempUpDown.Size = new System.Drawing.Size(56, 20);
            this.finalTempUpDown.TabIndex = 4;
            this.finalTempUpDown.ThousandsSeparator = true;
            this.finalTempUpDown.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // label3
            // 
            label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(3, 67);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(108, 13);
            label3.TabIndex = 4;
            label3.Text = "Temperature Interval:";
            // 
            // label2
            // 
            label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(3, 35);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(78, 13);
            label2.TabIndex = 2;
            label2.Text = "Total Enthalpy:";
            // 
            // label1
            // 
            label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(3, 6);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(75, 13);
            label1.TabIndex = 0;
            label1.Text = "Display Name:";
            // 
            // label8
            // 
            label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(3, 97);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(106, 13);
            label8.TabIndex = 6;
            label8.Text = "Distribution Function:";
            // 
            // tableLayoutPanel3
            // 
            tableLayoutPanel3.ColumnCount = 1;
            tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tableLayoutPanel3.Controls.Add(this.configurationTablePanel, 0, 0);
            tableLayoutPanel3.Controls.Add(flowLayoutPanel1, 0, 1);
            tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            tableLayoutPanel3.Name = "tableLayoutPanel3";
            tableLayoutPanel3.RowCount = 2;
            tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            tableLayoutPanel3.Size = new System.Drawing.Size(307, 159);
            tableLayoutPanel3.TabIndex = 1;
            // 
            // configurationTablePanel
            // 
            this.configurationTablePanel.AutoSize = true;
            this.configurationTablePanel.ColumnCount = 2;
            this.configurationTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.configurationTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.configurationTablePanel.Controls.Add(tempIntervalKelvinFlowPanel, 1, 2);
            this.configurationTablePanel.Controls.Add(label3, 0, 2);
            this.configurationTablePanel.Controls.Add(label2, 0, 1);
            this.configurationTablePanel.Controls.Add(label1, 0, 0);
            this.configurationTablePanel.Controls.Add(this.nameTextBox, 1, 0);
            this.configurationTablePanel.Controls.Add(label8, 0, 3);
            this.configurationTablePanel.Controls.Add(this.distributionFunctionComboBox, 1, 3);
            this.configurationTablePanel.Controls.Add(flowLayoutPanel2, 1, 1);
            this.configurationTablePanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.configurationTablePanel.Location = new System.Drawing.Point(3, 3);
            this.configurationTablePanel.Name = "configurationTablePanel";
            this.configurationTablePanel.RowCount = 4;
            this.configurationTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.configurationTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.configurationTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.configurationTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.configurationTablePanel.Size = new System.Drawing.Size(301, 117);
            this.configurationTablePanel.TabIndex = 0;
            // 
            // nameTextBox
            // 
            this.nameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource, "Name", true));
            this.nameTextBox.Location = new System.Drawing.Point(117, 3);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(181, 20);
            this.nameTextBox.TabIndex = 1;
            this.nameTextBox.Text = "Transition";
            // 
            // distributionFunctionComboBox
            // 
            this.distributionFunctionComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.distributionFunctionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.distributionFunctionComboBox.FormattingEnabled = true;
            this.distributionFunctionComboBox.Location = new System.Drawing.Point(117, 93);
            this.distributionFunctionComboBox.Name = "distributionFunctionComboBox";
            this.distributionFunctionComboBox.Size = new System.Drawing.Size(181, 21);
            this.distributionFunctionComboBox.TabIndex = 5;
            this.distributionFunctionComboBox.SelectedValueChanged += new System.EventHandler(this.distributionFunctionComboBox_SelectedValueChanged);
            // 
            // flowLayoutPanel2
            // 
            flowLayoutPanel2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            flowLayoutPanel2.AutoSize = true;
            flowLayoutPanel2.Controls.Add(this.totalEnthalpyUpDown);
            flowLayoutPanel2.Location = new System.Drawing.Point(117, 29);
            flowLayoutPanel2.Name = "flowLayoutPanel2";
            flowLayoutPanel2.Size = new System.Drawing.Size(68, 26);
            flowLayoutPanel2.TabIndex = 7;
            // 
            // totalEnthalpyUpDown
            // 
            this.totalEnthalpyUpDown.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.totalEnthalpyUpDown.AutoSize = true;
            this.totalEnthalpyUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bindingSource, "TotalEnthalpy", true));
            this.totalEnthalpyUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.totalEnthalpyUpDown.Location = new System.Drawing.Point(3, 3);
            this.totalEnthalpyUpDown.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.totalEnthalpyUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.totalEnthalpyUpDown.Name = "totalEnthalpyUpDown";
            this.totalEnthalpyUpDown.Size = new System.Drawing.Size(62, 20);
            this.totalEnthalpyUpDown.TabIndex = 2;
            this.totalEnthalpyUpDown.ThousandsSeparator = true;
            this.totalEnthalpyUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // flowLayoutPanel1
            // 
            flowLayoutPanel1.AutoSize = true;
            flowLayoutPanel1.Controls.Add(this.deleteButton);
            flowLayoutPanel1.Location = new System.Drawing.Point(3, 126);
            flowLayoutPanel1.Name = "flowLayoutPanel1";
            flowLayoutPanel1.Size = new System.Drawing.Size(135, 29);
            flowLayoutPanel1.TabIndex = 1;
            // 
            // deleteButton
            // 
            this.deleteButton.AutoSize = true;
            this.deleteButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.deleteButton.Location = new System.Drawing.Point(3, 3);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(129, 23);
            this.deleteButton.TabIndex = 0;
            this.deleteButton.Text = "Remove This Transition";
            this.deleteButton.UseVisualStyleBackColor = false;
            // 
            // mainGroupBox
            // 
            this.mainGroupBox.BackColor = System.Drawing.SystemColors.ControlLight;
            this.mainGroupBox.Controls.Add(tableLayoutPanel3);
            this.mainGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGroupBox.Location = new System.Drawing.Point(0, 0);
            this.mainGroupBox.Name = "mainGroupBox";
            this.mainGroupBox.Size = new System.Drawing.Size(313, 178);
            this.mainGroupBox.TabIndex = 0;
            this.mainGroupBox.TabStop = false;
            this.mainGroupBox.Text = "Transition";
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // TransitionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.mainGroupBox);
            this.MinimumSize = new System.Drawing.Size(315, 180);
            this.Name = "TransitionControl";
            this.Size = new System.Drawing.Size(313, 178);
            tempIntervalKelvinFlowPanel.ResumeLayout(false);
            tempIntervalKelvinFlowPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.initialTempUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.finalTempUpDown)).EndInit();
            tableLayoutPanel3.ResumeLayout(false);
            tableLayoutPanel3.PerformLayout();
            this.configurationTablePanel.ResumeLayout(false);
            this.configurationTablePanel.PerformLayout();
            flowLayoutPanel2.ResumeLayout(false);
            flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.totalEnthalpyUpDown)).EndInit();
            flowLayoutPanel1.ResumeLayout(false);
            flowLayoutPanel1.PerformLayout();
            this.mainGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox mainGroupBox;
		private System.Windows.Forms.NumericUpDown totalEnthalpyUpDown;
		private System.Windows.Forms.NumericUpDown initialTempUpDown;
		private System.Windows.Forms.NumericUpDown finalTempUpDown;
		private System.Windows.Forms.ComboBox distributionFunctionComboBox;
		private System.Windows.Forms.BindingSource bindingSource;
		private System.Windows.Forms.TextBox nameTextBox;
		private System.Windows.Forms.TableLayoutPanel configurationTablePanel;
		private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.ErrorProvider errorProvider;
    }
}
