﻿using System.Windows.Forms;

namespace GenericThermalAnalyzer.Gui
{
	/// <summary>
	///     The window that contains various help info for user. This class cannot be inherited.
	/// </summary>
	internal partial class HelpWindow : Form
	{
		/// <summary>
		///     Initializes a new instance of <see cref="HelpWindow" /> class.
		/// </summary>
		public HelpWindow()
		{
			InitializeComponent();
		}

		private void exitButton_Click(object sender, System.EventArgs e) => Dispose();
	}
}