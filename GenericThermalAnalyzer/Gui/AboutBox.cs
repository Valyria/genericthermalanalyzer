﻿using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace GenericThermalAnalyzer.Gui
{
	/// <summary>
	///     The window that contains a basic summary information about the application. This class cannot be inherited.
	/// </summary>
	internal sealed partial class AboutBox : Form
	{
		/// <summary>
		///     Creates a new instance of <see cref="AboutBox" /> class.
		/// </summary>
		public AboutBox()
		{
			InitializeComponent();
			Text = AssemblyTitle;
			labelProductName.Text = AssemblyProduct;
			labelVersion.Text = $"Version {AssemblyVersion}";
			labelCopyright.Text = AssemblyCopyright;
			labelCompanyName.Text = AssemblyCompany;
			textBoxDescription.Text = AssemblyDescription;
		}

		private static string AssemblyTitle
		{
			get
			{
				var attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
				if (attributes.Length <= 0) return Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
				var titleAttribute = (AssemblyTitleAttribute) attributes[0];
				return titleAttribute.Title != ""
					? titleAttribute.Title
					: Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
			}
		}

		private static string AssemblyVersion => Assembly.GetExecutingAssembly().GetName().Version.ToString();

		private static string AssemblyDescription
		{
			get
			{
				var attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
				return attributes.Length == 0 ? "" : ((AssemblyDescriptionAttribute) attributes[0]).Description;
			}
		}

		private static string AssemblyProduct
		{
			get
			{
				var attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
				return attributes.Length == 0 ? "" : ((AssemblyProductAttribute) attributes[0]).Product;
			}
		}

		private static string AssemblyCopyright
		{
			get
			{
				var attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
				return attributes.Length == 0 ? "" : ((AssemblyCopyrightAttribute) attributes[0]).Copyright;
			}
		}

		private static string AssemblyCompany
		{
			get
			{
				var attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
				return attributes.Length == 0 ? "" : ((AssemblyCompanyAttribute) attributes[0]).Company;
			}
		}

		private void okButton_Click(object sender, EventArgs e) => Dispose();
	}
}