﻿using System;
using System.Windows.Forms;
using JetBrains.Annotations;

namespace GenericThermalAnalyzer.Gui
{
	/// <summary>
	///     A message box that allows the user to paste an input data. This class cannot be inherited.
	/// </summary>
	internal sealed partial class PasteDataWindow : Form
	{
		private static readonly PasteDataWindow Instance = new PasteDataWindow();

		private PasteDataWindow()
		{
			InitializeComponent();
		}

		/// <summary>
		///     Shows the message box that prompts the user to paste an input data.
		/// </summary>
		/// <returns>An input data pasted by user or <c>null</c> if no input was given.</returns>
		/// <exception cref="T:System.InvalidOperationException">
		///     The form being shown is already visible.-or- The form being shown
		///     is disabled.-or- The form being shown is not a top-level window.-or- The form being shown as a dialog box is
		///     already a modal form.-or-The current process is not running in user interactive mode (for more information, see
		///     <see cref="P:System.Windows.Forms.SystemInformation.UserInteractive" />).
		/// </exception>
		[CanBeNull]
		public static string Display()
		{
			Instance.ShowDialog();
			return Instance.inputTextBox.Text;
		}

		private void clearTextBoxButton_Click(object sender, EventArgs e)
		{
			inputTextBox.Clear();
		}
	}
}