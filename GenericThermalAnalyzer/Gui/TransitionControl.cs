﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Forms;
using GenericThermalAnalyzer.DistributionFunctions.Abstract;
using GenericThermalAnalyzer.ElementLibrary.Abstract;
using GenericThermalAnalyzer.ElementLibrary.Concrete;
using GenericThermalAnalyzer.Transition.Abstract;
using GenericThermalAnalyzer.Transition.Concrete;
using JetBrains.Annotations;

namespace GenericThermalAnalyzer.Gui
{
	/// <summary>
	///     The control that allows the user to configure the transition. This class cannot be inherited.
	/// </summary>
	internal sealed partial class TransitionControl : UserControl
	{
		private static readonly ILibrary<IDistributionFunction> DistributionFunctionsLibrary =
			new Library<IDistributionFunction>();

		private static int _instanceCounter = 1;

		private readonly int _configurationTableCoreRowsCount;

		private readonly List<Control> _dynamicControlsCache = new List<Control>();

		/// <summary>
		///     Initializes a new instance of <see cref="TransitionControl" /> class.
		/// </summary>
		public TransitionControl()
		{
			InitializeComponent();

			// To know exactly how many original rows are there in the configurationTablePanel.
			_configurationTableCoreRowsCount = configurationTablePanel.RowCount;

			// Subscribe to the events.

			nameTextBox.TextChanged += (sender, args) => mainGroupBox.Text = nameTextBox.Text;

			deleteButton.Click += (sender, args) =>
			{
				Parent.Controls.Remove(this);
				Dispose();
			};

			initialTempUpDown.ValueChanged += (sender, args) =>
			{
				finalTempUpDown.Minimum = initialTempUpDown.Value + 1;

				// A workaround to avoid being flooded by exceptions concerning argument out of range.
				Transition.FinalTemperature = (double) finalTempUpDown.Value;
			};

			// Initialize the rest of this component.

			Transition = new PhaseTransition {Name = $"Transition #{_instanceCounter++}"};

			var distFunctionsNames = DistributionFunctionsLibrary.GetElementsList();
			distFunctionsNames.Sort();
			distributionFunctionComboBox.DataSource = distFunctionsNames;
			distributionFunctionComboBox_SelectedValueChanged(
				null, EventArgs.Empty); // Force the invoke now to update the property.
		}

		/// <summary>
		///     Gets or sets the underlying implementation of <see cref="ITransition" />
		///     this control represents.
		/// </summary>
		/// <exception cref="ArgumentNullException"><paramref name="value" /> is <c>null</c>.</exception>
		[NotNull]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public ITransition Transition
		{
			// ReSharper disable once AssignNullToNotNullAttribute
			get => bindingSource.DataSource as ITransition;
			set
			{
				if (value == null)
					throw new ArgumentNullException(
						nameof(value), $"The underlying implementation of {nameof(ITransition)} must not be null.");

				if (value == Transition) return;

				bindingSource.DataSource = null;
				bindingSource.DataSource = value;
			}
		}

		private void distributionFunctionComboBox_SelectedValueChanged(object sender, EventArgs e)
		{
			var distFunc = Transition.DistributionFunction =
				DistributionFunctionsLibrary.GetElement((string) distributionFunctionComboBox.SelectedItem);

			// NOTE: This algorithm assumes the auto generated controls are always at the bottom of the table,
			// starting at the _configurationTableCoreRowsCount index.

			// Remove any dynamically added rows by restoring the original rows count.
			configurationTablePanel.RowCount = _configurationTableCoreRowsCount;

			// Dispose and forget all the dynamically added controls.
			foreach (var control in _dynamicControlsCache)
			{
				control.Dispose();
				if (control is NumericUpDown upDown)
					upDown.ValueChanged -= DynamicTextBoxOnTextChanged;
			}

			_dynamicControlsCache.Clear();

			// Restore the default height of the control.
			Height = DefaultSize.Height;

			// Get the list of configurable parameters.
			var parameters = distFunc.GetParameters();
			if (parameters.Count == 0) return;

			// Create the rows for dynamic controls at the bottom of the table.

			int currentRow = _configurationTableCoreRowsCount;

			foreach (string parameter in parameters)
			{
				var label = new Label
				{
					Anchor = AnchorStyles.Left,
					AutoSize = true,
					Text = $"{parameter}:"
				};

				var textBox = new TextBox
				{
					Name = parameter,
					Text = distFunc.GetParam(parameter).ToString(CultureInfo.InvariantCulture),
					Width = 60
				};

				// Subscribe to the event.
				textBox.TextChanged += DynamicTextBoxOnTextChanged;

				// Insert the controls at the bottom of the table and cache them.
				configurationTablePanel.Controls.Add(label, 0, currentRow);
				configurationTablePanel.Controls.Add(textBox, 1, currentRow++);

				_dynamicControlsCache.Add(label);
				_dynamicControlsCache.Add(textBox);

				// Adjust the height of the entire control.
				Height += textBox.Height + textBox.Margin.Bottom + textBox.Margin.Top;
			}
		}

		private void DynamicTextBoxOnTextChanged(object sender, EventArgs eventArgs)
		{
			if (!(sender is TextBox textBox)) return;

			try
			{
				if (!double.TryParse(textBox.Text, out double value))
				{
					errorProvider.SetError(textBox, "Please provide a correct numeric value.");
					return;
				}

				Transition.DistributionFunction.SetParam(textBox.Name, value);
				errorProvider.SetError(textBox, null);
			}
			catch (ArgumentException e)
			{
				errorProvider.SetError(textBox, e.Message);
			}
			catch (Exception e)
			{
				Debug.Fail(
					"Error setting parameter. This should not have happened. If you see this message, contact Karol Domański.\n\n" +
					$"{e.Message}\n\n{e.StackTrace}");
			}
		}
	}
}