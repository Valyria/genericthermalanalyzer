﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GenericThermalAnalyzer.Input.Concrete;
using GenericThermalAnalyzer.Output.Concrete;
using GenericThermalAnalyzer.Transition.Abstract;
using GenericThermalAnalyzer.Utility;
using JetBrains.Annotations;

namespace GenericThermalAnalyzer.Gui
{
	/// <summary>
	///     The main window of the application. This class cannot be inherited.
	/// </summary>
	internal sealed partial class MainWindow : Form
	{
		private readonly DocumentReaderContext _documentReader = new DocumentReaderContext();
		private readonly ExportContext _exportTable = new ExportContext();

		/// <summary>
		///     Initializes a new instance of <see cref="MainWindow" /> class.
		/// </summary>
		public MainWindow()
		{
			InitializeComponent();

			// This controls the smoothness of the curve corners. A default value is 0.8.
			chart.Series[0]["LineTension"] = "0.0";

			transitionsFlowPanel.AutoSize = true;
			transitionsFlowPanel.ControlAdded += (sender, args) => noTransitionsLabel.Visible = false;
			transitionsFlowPanel.ControlRemoved += (sender, args) =>
			{
				if (transitionsFlowPanel.Controls.Count == 1)
					noTransitionsLabel.Visible = true;

				var dataTable = DataTable;
				if (dataTable == null) return;

				if (!(args.Control is TransitionControl control)) return;

				var column = dataTable.Columns[$"{control.Transition.Name}Enthalpy"];
				if (column == null) return;

				dataTable.Columns.Remove(column);
			};

			string importFormats = "The supported file formats are: " + string
																		.Join(", ", _documentReader.GetSupportedFormats.Select(format => format.ToUpper()))
																		.TrimEnd(',') + ".";

			string exportFormats = "The supported file formats are: " + string
																		.Join(", ", _exportTable.GetSupportedFormats.Select(format => format.ToUpper()))
																		.TrimEnd(',') + ".";

			importDataToolStripMenuItem.ToolTipText = importFormats;
			exportDataToolStripMenuItem.ToolTipText = exportFormats;

			// Add the extensions filters to the dialogs.
			importFormats = "All files|*.*|" + string
											   .Join("|", _documentReader.GetSupportedFormats.Select(format => $"{format.ToUpper()}|*.{format}"))
											   .TrimEnd('|');

			exportFormats = string.Join("|", _exportTable.GetSupportedFormats.Select(format => $"{format.ToUpper()}|*.{format}"))
								  .TrimEnd('|');

			inputOpenFileDialog.Filter = importFormats;
			exportOpenFileDialog.Filter = exportFormats;
		}

		/// <summary>
		///     Gets all the phase transitions defined by the user or returns <c>null</c> if no transitions
		///     are defined.
		/// </summary>
		[CanBeNull]
		private ICollection<ITransition> Transitions
		{
			get
			{
				// The child at index 0 is always the noTransitionsLabel.
				if (transitionsFlowPanel.Controls.Count == 1) return null;

				var transitions = new List<ITransition>(transitionsFlowPanel.Controls.Count);

				for (int i = 1; i < transitionsFlowPanel.Controls.Count; i++)
				{
					var control = transitionsFlowPanel.Controls[i] as TransitionControl;

					if (control == null)
						Debug.Fail($"Illegal child of the {nameof(transitionsFlowPanel)} control.\n" +
								   $"Expected {nameof(TransitionControl)}, got {transitionsFlowPanel.Controls[i].GetType()}");

					transitions.Add(control.Transition);
				}

				return transitions;
			}
		}

		/// <summary>
		///     Gets or sets the <c>DataTable</c> that's currently being processed by the application.
		/// </summary>
		[CanBeNull]
		private DataTable DataTable
		{
			get => dataGridView.DataSource as DataTable;
			set
			{
				runAnalysisToolStripMenuItem.Enabled = value != null;
				exportDataToolStripMenuItem.Enabled = value != null;

				dataGridView.DataSource = value;
				chart.Visible = false;
				chart.DataSource = null;
			}
		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e) => Application.Exit();

		private void aboutToolStripMenuItem_Click(object sender, EventArgs e) => new AboutBox().ShowDialog();

		private void addTransitionButton_Click(object sender, EventArgs e)
		{
			Cursor.Current = Cursors.WaitCursor;
			transitionsFlowPanel.Controls.Add(new TransitionControl());
			Cursor.Current = Cursors.Default;
		}

		private void importDataToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (inputOpenFileDialog.ShowDialog() != DialogResult.OK) return;

			InputData inputData = null;

			try
			{
				inputData = new InputData(inputOpenFileDialog.FileName);
				_documentReader.ReadDocument(inputData);
				DataTable = inputData.DataTable;
			}
			catch (FileNotFoundException exception)
			{
				MessageBox.Show($"{exception.Message}", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			catch (Exception exception)
			{
				MessageBox.Show(
					$"A fatal unresolved error has occurred while loading a file \"{inputData?.Name}.{inputData?.Extension}\": {exception.Message}",
					"File Not Loaded", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void exportDataToolStripMenuItem_Click(object sender, EventArgs e)
		{
			var dataTable = DataTable;
			
			if (dataTable == null) return;
			if (exportOpenFileDialog.ShowDialog() != DialogResult.OK) return;

			ExportData exportData = null;

			try
			{
				exportData = new ExportData(exportOpenFileDialog.FileName);
				exportData.DataTable = dataTable.Copy();
				var export2Xls = new Export2Xls();
				export2Xls.ExportTable(DataTable, exportData.FilePath, exportData.Name);
			}
			catch (FileNotFoundException exception)
			{
				MessageBox.Show($"{exception.Message}", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			catch (Exception exception)
			{
				MessageBox.Show(
					$"A fatal unresolved error has occurred while loading a file \"{exportData?.Name}.{exportData?.Extension}\": {exception.Message}",
					"File Not Loaded", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void runAnalysisToolStripMenuItem_Click(object sender, EventArgs e)
		{
			var dataTable = DataTable;

			if (dataTable == null)
			{
				MessageBox.Show(
					"The analysis cannot be performed on non-existing data.",
					"No Input Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

			var transitions = Transitions;

			if (transitions == null)
			{
				MessageBox.Show(
					"There aren't any phase transitions defined. The analysis cannot be performed.",
					"No Transitions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

			transitionsFlowPanel.Focus(); // A workaround to force the update of the properties bound to the controls. Issue #5
			Cursor.Current = Cursors.WaitCursor;

			try
			{
				dataTable = DataPointDensifier.DensifyDataPoints(dataTable);
				dataTable = EnthalpyDistributor.DistributeBasicEnthalpy(dataTable);
				DataTable = dataTable;
				foreach (var transition in transitions)
					transition.ComputeEnthalpyTable(dataTable);
				EnthalpyCombiner.CombineEnthalpies(dataTable, false);

				// Bind data to the chart.
				chart.DataSource = dataTable;
				chart.Series[0].XValueMember = "Temperature";
				chart.Series[0].YValueMembers = EnthalpyCombiner.TotalEnthalpyColumn;
				chart.DataBind();
				chart.Visible = true;
			}
			catch (Exception ex)
			{
				MessageBox.Show(
					$"A fatal error has occurred while performing the analysis: {ex.Message}\n\n{ex.StackTrace}",
					"Analysis Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

			Cursor.Current = Cursors.Default;
		}

		private void pasteDataToolStripMenuItem_Click(object sender, EventArgs e)
		{
			string input = PasteDataWindow.Display();
			if (string.IsNullOrEmpty(input)) return;

			// Since IDocumentReader requires a file to read, we need to make a temporary dummy file.
			string tmpFilePath = AppDomain.CurrentDomain.BaseDirectory + Path.PathSeparator + "tmpFile.txt";

			try
			{
				File.WriteAllText(tmpFilePath, input, Encoding.UTF8);
			}
			catch (Exception ex)
			{
				MessageBox.Show(
					$"Unable to create or write to a temporary dummy text file: {ex.Message}",
					"Pasting Input Data Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);

				try
				{
					File.Delete(tmpFilePath);
				}
				catch (Exception)
				{
					// ignored
				}

				return;
			}

			try
			{
				DataTable = new TxtDocumentReader().ReadDocument(tmpFilePath);
			}
			catch (Exception ex)
			{
				MessageBox.Show(
					$"Unable to open or read from a temporary dummy text file: {ex.Message}",
					"Pasting Input Data Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			finally
			{
				try
				{
					File.Delete(tmpFilePath);
				}
				catch (Exception)
				{
					// ignored
				}
			}
		}

		private void unitsToolStripMenuItem_Click(object sender, EventArgs e) => new HelpWindow().ShowDialog();
	}
}