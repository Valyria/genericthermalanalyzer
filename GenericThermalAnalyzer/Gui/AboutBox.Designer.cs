﻿namespace GenericThermalAnalyzer.Gui
{
	partial class AboutBox
	{
		/// <summary>
		/// Wymagana zmienna projektanta.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Wyczyść wszystkie używane zasoby.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Kod generowany przez Projektanta formularzy systemu Windows

		/// <summary>
		/// Metoda wymagana do obsługi projektanta — nie należy modyfikować
		/// jej zawartości w edytorze kodu.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.Button okButton;
			System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
			System.Windows.Forms.PictureBox pictureBox2;
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutBox));
			System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
			System.Windows.Forms.PictureBox pictureBox1;
			System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
			this.labelProductName = new System.Windows.Forms.Label();
			this.labelVersion = new System.Windows.Forms.Label();
			this.labelCopyright = new System.Windows.Forms.Label();
			this.labelCompanyName = new System.Windows.Forms.Label();
			this.textBoxDescription = new System.Windows.Forms.TextBox();
			okButton = new System.Windows.Forms.Button();
			tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			pictureBox2 = new System.Windows.Forms.PictureBox();
			tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			pictureBox1 = new System.Windows.Forms.PictureBox();
			tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
			tableLayoutPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(pictureBox2)).BeginInit();
			tableLayoutPanel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(pictureBox1)).BeginInit();
			tableLayoutPanel3.SuspendLayout();
			this.SuspendLayout();
			// 
			// okButton
			// 
			okButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
			okButton.AutoSize = true;
			okButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			okButton.Location = new System.Drawing.Point(190, 281);
			okButton.Name = "okButton";
			okButton.Size = new System.Drawing.Size(82, 23);
			okButton.TabIndex = 24;
			okButton.Text = "&Acknowledge";
			okButton.Click += new System.EventHandler(this.okButton_Click);
			// 
			// tableLayoutPanel
			// 
			tableLayoutPanel.ColumnCount = 1;
			tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			tableLayoutPanel.Controls.Add(this.labelProductName, 0, 0);
			tableLayoutPanel.Controls.Add(this.labelVersion, 0, 1);
			tableLayoutPanel.Controls.Add(this.labelCopyright, 0, 2);
			tableLayoutPanel.Controls.Add(this.labelCompanyName, 0, 3);
			tableLayoutPanel.Controls.Add(this.textBoxDescription, 0, 4);
			tableLayoutPanel.Controls.Add(okButton, 0, 5);
			tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			tableLayoutPanel.Location = new System.Drawing.Point(168, 3);
			tableLayoutPanel.Name = "tableLayoutPanel";
			tableLayoutPanel.RowCount = 6;
			tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			tableLayoutPanel.Size = new System.Drawing.Size(275, 307);
			tableLayoutPanel.TabIndex = 0;
			// 
			// labelProductName
			// 
			this.labelProductName.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.labelProductName.AutoSize = true;
			this.labelProductName.Location = new System.Drawing.Point(6, 0);
			this.labelProductName.Margin = new System.Windows.Forms.Padding(6, 0, 3, 8);
			this.labelProductName.MaximumSize = new System.Drawing.Size(0, 17);
			this.labelProductName.Name = "labelProductName";
			this.labelProductName.Size = new System.Drawing.Size(73, 13);
			this.labelProductName.TabIndex = 19;
			this.labelProductName.Text = "Product name";
			this.labelProductName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// labelVersion
			// 
			this.labelVersion.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.labelVersion.AutoSize = true;
			this.labelVersion.Location = new System.Drawing.Point(6, 21);
			this.labelVersion.Margin = new System.Windows.Forms.Padding(6, 0, 3, 8);
			this.labelVersion.MaximumSize = new System.Drawing.Size(0, 17);
			this.labelVersion.Name = "labelVersion";
			this.labelVersion.Size = new System.Drawing.Size(42, 13);
			this.labelVersion.TabIndex = 0;
			this.labelVersion.Text = "Version";
			this.labelVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// labelCopyright
			// 
			this.labelCopyright.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.labelCopyright.AutoSize = true;
			this.labelCopyright.Location = new System.Drawing.Point(6, 42);
			this.labelCopyright.Margin = new System.Windows.Forms.Padding(6, 0, 3, 8);
			this.labelCopyright.MaximumSize = new System.Drawing.Size(0, 17);
			this.labelCopyright.Name = "labelCopyright";
			this.labelCopyright.Size = new System.Drawing.Size(51, 13);
			this.labelCopyright.TabIndex = 21;
			this.labelCopyright.Text = "Copyright";
			this.labelCopyright.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// labelCompanyName
			// 
			this.labelCompanyName.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.labelCompanyName.AutoSize = true;
			this.labelCompanyName.Location = new System.Drawing.Point(6, 63);
			this.labelCompanyName.Margin = new System.Windows.Forms.Padding(6, 0, 3, 8);
			this.labelCompanyName.MaximumSize = new System.Drawing.Size(0, 17);
			this.labelCompanyName.Name = "labelCompanyName";
			this.labelCompanyName.Size = new System.Drawing.Size(51, 13);
			this.labelCompanyName.TabIndex = 22;
			this.labelCompanyName.Text = "Company";
			this.labelCompanyName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// textBoxDescription
			// 
			this.textBoxDescription.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxDescription.Location = new System.Drawing.Point(6, 87);
			this.textBoxDescription.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
			this.textBoxDescription.Multiline = true;
			this.textBoxDescription.Name = "textBoxDescription";
			this.textBoxDescription.ReadOnly = true;
			this.textBoxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.textBoxDescription.Size = new System.Drawing.Size(266, 188);
			this.textBoxDescription.TabIndex = 23;
			this.textBoxDescription.TabStop = false;
			this.textBoxDescription.Text = "Description";
			// 
			// pictureBox2
			// 
			pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
			pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			pictureBox2.Location = new System.Drawing.Point(3, 3);
			pictureBox2.Name = "pictureBox2";
			pictureBox2.Size = new System.Drawing.Size(153, 147);
			pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			pictureBox2.TabIndex = 2;
			pictureBox2.TabStop = false;
			// 
			// tableLayoutPanel2
			// 
			tableLayoutPanel2.ColumnCount = 1;
			tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			tableLayoutPanel2.Controls.Add(pictureBox1, 0, 1);
			tableLayoutPanel2.Controls.Add(pictureBox2, 0, 0);
			tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
			tableLayoutPanel2.Name = "tableLayoutPanel2";
			tableLayoutPanel2.RowCount = 2;
			tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			tableLayoutPanel2.Size = new System.Drawing.Size(159, 307);
			tableLayoutPanel2.TabIndex = 3;
			// 
			// pictureBox1
			// 
			pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
			pictureBox1.Location = new System.Drawing.Point(3, 156);
			pictureBox1.Name = "pictureBox1";
			pictureBox1.Size = new System.Drawing.Size(153, 148);
			pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			pictureBox1.TabIndex = 2;
			pictureBox1.TabStop = false;
			// 
			// tableLayoutPanel3
			// 
			tableLayoutPanel3.ColumnCount = 2;
			tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37F));
			tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63F));
			tableLayoutPanel3.Controls.Add(tableLayoutPanel2, 0, 0);
			tableLayoutPanel3.Controls.Add(tableLayoutPanel, 1, 0);
			tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			tableLayoutPanel3.Location = new System.Drawing.Point(9, 9);
			tableLayoutPanel3.Name = "tableLayoutPanel3";
			tableLayoutPanel3.RowCount = 1;
			tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			tableLayoutPanel3.Size = new System.Drawing.Size(446, 313);
			tableLayoutPanel3.TabIndex = 2;
			// 
			// AboutBox
			// 
			this.AcceptButton = okButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(464, 331);
			this.Controls.Add(tableLayoutPanel3);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "AboutBox";
			this.Padding = new System.Windows.Forms.Padding(9);
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "AboutBox";
			tableLayoutPanel.ResumeLayout(false);
			tableLayoutPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(pictureBox2)).EndInit();
			tableLayoutPanel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(pictureBox1)).EndInit();
			tableLayoutPanel3.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label labelCompanyName;
		private System.Windows.Forms.Label labelCopyright;
		private System.Windows.Forms.Label labelVersion;
		private System.Windows.Forms.Label labelProductName;
		private System.Windows.Forms.TextBox textBoxDescription;
	}
}
