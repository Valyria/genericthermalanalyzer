﻿using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.Office.Interop.Word;
using DataTable = System.Data.DataTable;

namespace GenericThermalAnalyzer.Utility
{
	public class DataPointDensifier
	{
		/// <summary>
		/// Use simple two-point interpolation to generate denser data points
		/// </summary>
		/// <param name="inputData">Basic datatable</param>
		/// <returns>DataTable with additional data points</returns>
		public static DataTable DensifyDataPoints(DataTable inputData)
		{
			if (!inputData.Columns.Contains("Temperature"))
			{
				throw new Exception("Missing 'Temperature' column");
			}
			DataTable outputData = new DataTable("Densified");

			foreach (DataColumn column in inputData.Columns)
			{
				outputData.Columns.Add(column.ColumnName, column.DataType);
			}

			double currentTemperature = 1, maximumTemperature = (double)inputData.Rows[inputData.Rows.Count - 1]["Temperature"],
				previousTemperature = 0;
			int currentIndex = 0;

			double nextTemperature = (double) inputData.Rows[currentIndex]["Temperature"];
			while (currentTemperature <= maximumTemperature)
			{
				while (currentTemperature > nextTemperature)
				{
					currentIndex += 1;
					if (currentIndex >= inputData.Rows.Count)
						break;
					previousTemperature = nextTemperature;
					nextTemperature = (double) inputData.Rows[currentIndex]["Temperature"];
				}
				if (currentIndex >= inputData.Rows.Count)
					break;

				double prevFrac = (nextTemperature - currentTemperature) / (nextTemperature - previousTemperature);
				double nextFrac = 1 - prevFrac;
				DataRow newRow = outputData.NewRow();
				foreach (DataColumn column in inputData.Columns)
				{
					var name = column.ColumnName;
					if (name == "Temperature")
					{
						newRow[name] = currentTemperature;
						continue;
					}
					double nextValue = (double)inputData.Rows[currentIndex][name];
					double prevValue = 0;
					if (currentIndex > 0)
					{
						prevValue = (double)inputData.Rows[currentIndex - 1][name];
					}
					newRow[name] = nextValue*nextFrac + prevValue * prevFrac;
				}
				outputData.Rows.Add(newRow);

				currentTemperature += 1;
			}
			return outputData;
		}
	}
}