﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace GenericThermalAnalyzer.Utility
{
    class EnthalpyDistributor
    {
        public static DataTable DistributeBasicEnthalpy(DataTable inputData)
        {
            DataTable distributedEnthalpy = new DataTable("Enthalpy");
            distributedEnthalpy.Columns.Add("Temperature", typeof(double));
			distributedEnthalpy.Columns.Add("Specific_heat", typeof(double));
			distributedEnthalpy.Columns.Add("BasicEnthalpy", typeof(double));

			double initialTemperature = 0, initialHeat = 0, totalEnthalpy = 0;
            double finalTemperature = (double)inputData.Rows[0]["Temperature"];
            double finalHeat = (double)inputData.Rows[0]["Specific_heat"];

            distributedEnthalpy.Rows.Add(new Object[]{
                finalTemperature,
				finalHeat,
                CalculateEnthalpy(initialTemperature, finalTemperature, (initialHeat+finalHeat)/2.0)
            });
            for (int i = 1; i < inputData.Rows.Count; i++)
            {
                initialTemperature = finalTemperature;
                initialHeat = finalHeat;
                finalTemperature = (double)inputData.Rows[i]["Temperature"];
                finalHeat = (double)inputData.Rows[i]["Specific_heat"];
				totalEnthalpy += CalculateEnthalpy(initialTemperature, finalTemperature, (initialHeat + finalHeat) / 2.0);
				distributedEnthalpy.Rows.Add(new Object[]{
                finalTemperature,
				finalHeat,
                totalEnthalpy
            });
            }

            return distributedEnthalpy;
        }

        private static double CalculateEnthalpy(double initialTemperature, double finalTemperature, double specificHeat)
        {
            return (finalTemperature - initialTemperature) * specificHeat;
        }
    }
}
