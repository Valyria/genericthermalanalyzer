﻿using System.Collections.Generic;
using System.Data;

namespace GenericThermalAnalyzer.Utility
{
	/// <summary>
	///     Use this utility to combine multiple enthalpy
	///     columns into one, containing total enthalpy
	/// </summary>
	public class EnthalpyCombiner
	{
		/// <summary>
		///     Total enthalpy column name
		/// </summary>
		public const string TotalEnthalpyColumn = "TotalEnthalpy";

		/// <summary>
		///     Combine multiple enthalpy columns into one
		///     Only columns containing "Enthalpy" word in
		///     their name and being of type "double" are
		///     considered valid columns. If total enthalpy
		///     column already exists it's dropped and then
		///     re-created.
		/// </summary>
		/// <param name="dataTable">DataTable to be processed</param>
		/// <param name="drop">
		///     Drop Enthalpy columns after processing
		///     is done (true by default)
		/// </param>
		public static void CombineEnthalpies(DataTable dataTable, bool drop = true)
		{
			var columns = dataTable.Columns;
			var enthalpyColumns = new List<string>();
			if (dataTable.Columns.Contains(TotalEnthalpyColumn))
				dataTable.Columns.Remove(TotalEnthalpyColumn);
			foreach (DataColumn column in columns)
				if (column.ColumnName.Contains("Enthalpy") && column.DataType == typeof(double))
					enthalpyColumns.Add(column.ColumnName);
			var totalEnthalpyColumn = new DataColumn(TotalEnthalpyColumn, typeof(double));
			dataTable.Columns.Add(totalEnthalpyColumn);
			foreach (DataRow row in dataTable.Rows)
			{
				double totalEnthalpy = 0.0;
				foreach (string enthalpyColumn in enthalpyColumns) totalEnthalpy += (double) row[enthalpyColumn];

				row[TotalEnthalpyColumn] = totalEnthalpy;
			}

			if (!drop) return;
			foreach (string column in enthalpyColumns) dataTable.Columns.Remove(column);
		}
	}
}