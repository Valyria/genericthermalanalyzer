﻿using GenericThermalAnalyzer.ElementLibrary.Abstract;

namespace GenericThermalAnalyzer.DistributionFunctions.Abstract
{
	/// <summary>
	///     Interface for distribution funcions, providing
	///     uniform way to provide information for f.e.
	///     enthalpy fraction depending on transition progress
	/// </summary>
	public interface IDistributionFunction : IElement
	{
		/// <summary>
		///     Return distribution value at given point, ranging from 0 to 1
		/// </summary>
		/// <param name="x">Value ranging from 0 to 1</param>
		/// <returns></returns>
		double Call(double x);
	}
}