﻿using System;
using GenericThermalAnalyzer.DistributionFunctions.Abstract;
using GenericThermalAnalyzer.ElementLibrary.Concrete;
using MathNet.Numerics;

namespace GenericThermalAnalyzer.DistributionFunctions.Concrete
{
	/// <summary>
	///     Implementation for normal distribution function
	/// </summary>
	public class NormalDistributionFunction : Element, IDistributionFunction
	{
		public NormalDistributionFunction()
		{
			AddConstraint("Scaling ratio <1..100>", ratio => ratio >= 1.0 && ratio <= 100.0);
			SetParam("Scaling ratio <1..100>", 5.0);
			AddConstraint("Mean (0..1)", mean => mean > 0.0 && mean < 1.0);
			SetParam("Mean (0..1)", 0.5);
		}

		/// <inheritdoc />
		public double Call(double x)
		{
			double trimmed = Math.Min(1.0, Math.Max(0.0, x));
			if (x < 0.0)
				return 0.0;
			if (x > 1.0)
				return 1.0;
			double scale = GetParam("Scaling ratio <1..100>");
			double midpoint = GetParam("Mean (0..1)");
			double z = scale * (x - midpoint) / Math.Sqrt(2);
			return 0.5 * (1.0 + SpecialFunctions.Erf(z));
		}
	}
}