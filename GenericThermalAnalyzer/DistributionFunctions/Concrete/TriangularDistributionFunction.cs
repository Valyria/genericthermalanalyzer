﻿using System;
using GenericThermalAnalyzer.DistributionFunctions.Abstract;
using GenericThermalAnalyzer.ElementLibrary.Concrete;

namespace GenericThermalAnalyzer.DistributionFunctions.Concrete
{
	/// <summary>
	///     Implementation for triangular probability density function
	/// </summary>
	public class TriangularDistributionFunction : Element, IDistributionFunction
	{
		public TriangularDistributionFunction()
		{
			AddConstraint("Center <0..1>", center => center >= 0.0 && center <= 1.0);
			SetParam("Center <0..1>", 0.5);
		}

		/// <inheritdoc />
		public double Call(double x)
		{
			if (x <= 0)
				return 0;
			else if (x >= 1)
				return 1;
			double center = GetParam("Center <0..1>");
			double trimmed = Math.Min(1.0, Math.Max(0.0, x));
			if (trimmed <= center) return Math.Pow(trimmed, 2.0) / center;
			return 1.0 - Math.Pow(1.0 - trimmed, 2.0) / (1 - center);
		}
	}
}