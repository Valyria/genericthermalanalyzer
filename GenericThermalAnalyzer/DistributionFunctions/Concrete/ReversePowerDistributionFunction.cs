﻿using System;
using GenericThermalAnalyzer.DistributionFunctions.Abstract;
using GenericThermalAnalyzer.ElementLibrary.Concrete;

namespace GenericThermalAnalyzer.DistributionFunctions.Concrete
{
	/// <summary>
	///     Implementation for simple power functions
	/// </summary>
	public class ReversePowerDistributionFunction : Element, IDistributionFunction
	{
		public ReversePowerDistributionFunction()
		{
			AddConstraint("Power (-1..+inf)", power => power > -1.0);
			SetParam("Power (-1..+inf)", 1.0);
		}

		/// <inheritdoc />
		public double Call(double x)
		{
			double trimmed = Math.Min(1.0, Math.Max(0.0, x));
			double power = GetParam("Power (-1..+inf)");
			return 1 - Math.Pow(1 - trimmed, power + 1);
		}
	}
}