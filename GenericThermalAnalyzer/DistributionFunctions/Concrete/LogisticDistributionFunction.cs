﻿using System;
using GenericThermalAnalyzer.DistributionFunctions.Abstract;
using GenericThermalAnalyzer.ElementLibrary.Concrete;

namespace GenericThermalAnalyzer.DistributionFunctions.Concrete
{
	/// <summary>
	///     Implementation for logistic distribution function
	/// </summary>
	public class LogisticDistributionFunction : Element, IDistributionFunction
	{
		public LogisticDistributionFunction()
		{
			AddConstraint("Scaling ratio <1..100>", ratio => ratio >= 1.0 && ratio <= 100.0);
			SetParam("Scaling ratio <1..100>", 10.0);
			AddConstraint("Middle point (0..1)", mean => mean > 0.0 && mean < 1.0);
			SetParam("Middle point (0..1)", 0.5);
		}
		/// <inheritdoc />
		public double Call(double x)
		{
			double trimmed = Math.Min(1.0, Math.Max(0.0, x));
			if (x < 0.0)
				return 0.0;
			if (x > 1.0)
				return 1.0;
			double scale = GetParam("Scaling ratio <1..100>");
			double midpoint = GetParam("Middle point (0..1)");
			return 1.0 / (1.0 + Math.Exp((midpoint - x) * scale));
		}
	}
}