﻿using System;
using GenericThermalAnalyzer.DistributionFunctions.Abstract;
using GenericThermalAnalyzer.ElementLibrary.Concrete;

namespace GenericThermalAnalyzer.DistributionFunctions.Concrete
{
	/// <summary>
	///     Implementation for uniform probability density function
	/// </summary>
	public class FlatDistributionFunction : Element, IDistributionFunction
	{
		/// <inheritdoc />
		public double Call(double x)
		{
			double trimmed = Math.Min(1.0, Math.Max(0.0, x));
			return trimmed;
		}
	}
}