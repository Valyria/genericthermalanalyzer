﻿using System.Data;
using JetBrains.Annotations;

namespace GenericThermalAnalyzer.Input.Abstract
{
	public interface IDocumentReader
	{
		[NotNull]
		DataTable ReadDocument(string filePath);
	}
}