﻿using System;
using System.Data;
using System.IO;
using ExcelDataReader;
using GenericThermalAnalyzer.Input.Abstract;
using GenericThermalAnalyzer.Input.Exceptions;

namespace GenericThermalAnalyzer.Input.Concrete
{
	internal class XlsDocumentReader : IDocumentReader
	{
		public virtual DataTable ReadDocument(string filePath)
		{
			var dataTable = new DataTable();
			try
			{
				var fs = File.Open(filePath, FileMode.Open, FileAccess.Read);

				IExcelDataReader reader;

				string docType =
					filePath.Substring(filePath.LastIndexOf("."), filePath.Length - filePath.LastIndexOf("."));

				if (docType.ToLower().Equals(".xlsx"))
					reader = ExcelReaderFactory.CreateOpenXmlReader(fs);
				else
					reader = ExcelReaderFactory.CreateBinaryReader(fs);

				var result = reader.AsDataSet(new ExcelDataSetConfiguration
				{
					ConfigureDataTable = _ => new ExcelDataTableConfiguration
					{
						UseHeaderRow = false
					}
				});

				dataTable = result.Tables[0];
				dataTable.Columns[0].ColumnName = "Temperature";
				dataTable.Columns[1].ColumnName = "Specific_Heat";

				while (dataTable.Columns.Count > 2)
				{
					dataTable.Columns.RemoveAt(2);
				}

				DataTable dataTableCopy = dataTable.Copy();
				for (int i = dataTable.Rows.Count - 1; i >= 0; i--)
				{
					DataRow dataRow = dataTable.Rows[i];
					if (!double.TryParse(dataRow["Temperature"].ToString(), out double temperature) || !double.TryParse(dataRow["Specific_Heat"].ToString(), out double specific))
					{
						dataTable.Rows.Remove(dataRow);
					}
				}

				reader.Close();

				return dataTable;
			}
			catch (Exception ex)
			{
				throw new DocumentReadingException($"A fatal error occurred while reading a [XLS/XSLX] file \"{filePath}\": {ex.Message}\n\n{ex.StackTrace}");
			}
		}
	}
}