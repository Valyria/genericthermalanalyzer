﻿using System;
using System.Data;
using System.IO;
using GenericThermalAnalyzer.Input.Abstract;
using GenericThermalAnalyzer.Input.Exceptions;

namespace GenericThermalAnalyzer.Input.Concrete
{
	public class TxtDocumentReader : IDocumentReader
	{
		/// <inheritdoc />
		public DataTable ReadDocument(string filePath)
		{
			try
			{
				var lines = File.ReadAllLines(filePath);
				var table = new DataTable("Text File Input Data");
				var separators = new[] {' ', ',', ';', '\t'};

				table.Columns.Add("Temperature", typeof(double));
				table.Columns.Add("Specific_heat", typeof(double));

				foreach (string line in lines)
				{
					var splits = line.Split(separators, StringSplitOptions.RemoveEmptyEntries);
					if (splits.Length != 2) continue; // We are interested only in two columns.

					if (!double.TryParse(splits[0], out double temperature))
						continue;

					if (!double.TryParse(splits[1], out double specificHeat))
						continue;

					table.Rows.Add(temperature, specificHeat);
				}

				return table;
			}
			catch (Exception ex)
			{
				throw new DocumentReadingException(
					$"A fatal error occurred while reading a text file \"{filePath}\": {ex.Message}\n\n{ex.StackTrace}");
			}
		}
	}
}