﻿using System.Data;
using System.IO;

namespace GenericThermalAnalyzer.Input.Concrete
{
	public class InputData
	{
		public InputData(string filePath)
		{
			if (string.IsNullOrEmpty(filePath) || !File.Exists(filePath))
				throw new FileNotFoundException($"The file \"{filePath}\" does not exist.");

			FilePath = filePath;
		}

		// ReSharper disable once PossibleNullReferenceException
		public string Extension => Path.GetExtension(FilePath).Substring(1).ToLower();

		/// <summary>
		///     Gets the name of the document file without its path and extension.
		/// </summary>

		public string Name => Path.GetFileNameWithoutExtension(FilePath);

		public string FilePath { get; }

		public DataTable DataTable { get; set; }
	}
}