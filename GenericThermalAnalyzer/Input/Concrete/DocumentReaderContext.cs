﻿using System;
using System.Collections.Generic;
using System.Data;
using GenericThermalAnalyzer.Input.Abstract;
using JetBrains.Annotations;

namespace GenericThermalAnalyzer.Input.Concrete
{
	public sealed class DocumentReaderContext
	{
		private readonly Dictionary<string, IDocumentReader> _readers;

		public DocumentReaderContext() => _readers = new Dictionary<string, IDocumentReader>(10)
		{
			{"xls", new XlsDocumentReader()},
			{"xlsx", new XlsDocumentReader()},
			{"csv", new CsvDocumentReader()},
			{"txt", new TxtDocumentReader()}
		};

		[NotNull]
		[ItemNotNull]
		public IEnumerable<string> GetSupportedFormats => _readers.Keys;

		[NotNull]
		public DataTable ReadDocument([NotNull] string filePath)
		{
			var inputFile = new InputData(filePath);
			ReadDocument(inputFile);
			return inputFile.DataTable;
		}

		public void ReadDocument([NotNull] InputData inputData)
		{
			if (inputData == null)
				throw new ArgumentNullException(nameof(inputData),
												"Cannot read the specified document because it's null.");

			IDocumentReader reader = GetDocumentReader(inputData.Extension);

			if (reader == null)
				throw new Exception("There is no document reader registered that is associated with " +
													 inputData.Extension.ToUpper() + " file format.");

			inputData.DataTable = reader.ReadDocument(inputData.FilePath);
		}


		public IDocumentReader GetDocumentReader([NotNull] string extension)
		{
			IDocumentReader documentReader;
			return _readers.TryGetValue(extension, out documentReader) ? documentReader : null;
		}

		[NotNull]
		public static string NormalizeExtension([NotNull] string extension)
		{
			if (string.IsNullOrWhiteSpace(extension))
				throw new ArgumentNullException(
					nameof(extension),
					"The specified file extension is either null, empty or consists only of white-space characters.");

			return extension.ToLower().Trim('.');
		}
	}
}