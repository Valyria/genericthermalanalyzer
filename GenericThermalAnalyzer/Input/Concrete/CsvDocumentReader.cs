﻿using System;
using System.Data;
using System.IO;
using GenericThermalAnalyzer.Input.Abstract;
using GenericThermalAnalyzer.Input.Exceptions;

namespace GenericThermalAnalyzer.Input.Concrete
{
	internal class CsvDocumentReader : IDocumentReader
	{
		public virtual DataTable ReadDocument(string filePath)
		{
			try
			{
				var dataTable = new DataTable();
				using (var streamReader = new StreamReader(filePath))
				{
					var headers = streamReader.ReadLine().Split(';');
					foreach (string header in headers) dataTable.Columns.Add(header, typeof(double));
					while (!streamReader.EndOfStream)
					{
						var rows = streamReader.ReadLine().Split(';');
						var dataRow = dataTable.NewRow();
						for (int counter = 0; counter < headers.Length; counter++)
							if (counter < rows.Length)
								dataRow[counter] = Convert.ToDouble(rows[counter]);
						dataTable.Rows.Add(dataRow);
					}
				}

				return dataTable;
			}
			catch (Exception ex)
			{
				throw new DocumentReadingException("A fatal error occurred while reading a text file. (" + filePath +")", ex);
			}
		}
	}
}