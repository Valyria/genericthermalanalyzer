﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Windows.Forms;

namespace GenericThermalAnalyzer.Input.Exceptions
{
	public abstract class DocumentException : Exception
	{
		protected DocumentException() { }

		protected DocumentException(string message) : base(message) { }

		protected DocumentException(string message, System.Exception inner) : base(message, inner) { }

		protected DocumentException(
			SerializationInfo info,
			StreamingContext context) : base(info, context) { }
	}

	[Serializable]
	public class DocumentReadingException : DocumentException
	{
		public DocumentReadingException() { }

		public DocumentReadingException(string message) : base(message) { }

		public DocumentReadingException(string message, Exception inner) : base(message, inner) { }

		protected DocumentReadingException(
			SerializationInfo info,
			StreamingContext context) : base(info, context) { }
	}
}