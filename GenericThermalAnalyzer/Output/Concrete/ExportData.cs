﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace GenericThermalAnalyzer.Output.Concrete
{
	class ExportData
	{

		public ExportData(string filePath)
		{

			FilePath = filePath;
		}

		// ReSharper disable once PossibleNullReferenceException
		public string Extension => Path.GetExtension(FilePath).Substring(1).ToLower();

		/// <summary>
		///     Gets the name of the document file without its path and extension.
		/// </summary>

		public string Name => Path.GetFileNameWithoutExtension(FilePath);

		public string FilePath { get; }

		public DataTable DataTable { get; set; }
	}
}