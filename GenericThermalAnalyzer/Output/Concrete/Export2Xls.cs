﻿using System;
using GenericThermalAnalyzer.Output.Abstract;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using OfficeExcel = Microsoft.Office.Interop.Excel;

namespace GenericThermalAnalyzer.Output.Concrete
{
	internal class Export2Xls : ITableExport
	{
		public virtual void ExportTable(DataTable dataTable, String path, String filename)
		{

			DataSet ds = new DataSet();
			DataTable dt = dataTable.Copy();
			ds.Tables.Add(dt);
			string strPath = path;
			int inHeaderLength = 3, inColumn = 0, inRow = 0;
			System.Reflection.Missing Default = System.Reflection.Missing.Value;
			//Create Excel File
			strPath += "";
			OfficeExcel.Application excelApp = new OfficeExcel.Application();
			OfficeExcel.Workbook excelWorkBook = excelApp.Workbooks.Add(1);
			foreach (DataTable dtbl in ds.Tables)
			{
				//Create Excel WorkSheet
				OfficeExcel.Worksheet excelWorkSheet = excelWorkBook.Sheets.Add(Default, excelWorkBook.Sheets[excelWorkBook.Sheets.Count], 1, Default);
				excelWorkSheet.Name = "Wyniki";//Name worksheet

				//Write Column Name
				for (int i = 0; i < dtbl.Columns.Count; i++)
					excelWorkSheet.Cells[inHeaderLength + 1, i + 1] = dtbl.Columns[i].ColumnName.ToUpper();

				//Write Rows
				for (int m = 0; m < dtbl.Rows.Count; m++)
				{
					for (int n = 0; n < dtbl.Columns.Count; n++)
					{
						inColumn = n + 1;
						inRow = inHeaderLength + 2 + m;
						excelWorkSheet.get_Range("A" + inRow.ToString(), "Z" + inRow.ToString()).Font.Color = System.Drawing.Color.White;
						excelWorkSheet.Cells[inRow, inColumn] = dtbl.Rows[m].ItemArray[n].ToString().Replace(',', '.');
						if (m % 3 == 0)
							excelWorkSheet.get_Range("A" + inRow.ToString(), "Z" + inRow.ToString()).Interior.Color = System.Drawing.ColorTranslator.FromHtml("#2A5340");
						else if (m % 3 == 1)
							excelWorkSheet.get_Range("A" + inRow.ToString(), "Z" + inRow.ToString()).Interior.Color = System.Drawing.ColorTranslator.FromHtml("#231f20");

						else
							excelWorkSheet.get_Range("A" + inRow.ToString(), "Z" + inRow.ToString()).Interior.Color = System.Drawing.ColorTranslator.FromHtml("#B01028");
					}

				}

				//Excel Header
				OfficeExcel.Range cellRang = excelWorkSheet.get_Range("A1", "Z3");
				cellRang.Merge(false);
				cellRang.Interior.Color = System.Drawing.Color.White;
				cellRang.Font.Color = System.Drawing.Color.Gray;
				cellRang.HorizontalAlignment = OfficeExcel.XlHAlign.xlHAlignCenter;
				cellRang.VerticalAlignment = OfficeExcel.XlVAlign.xlVAlignCenter;
				cellRang.Font.Size = 26;
				excelWorkSheet.Cells[1, 1] = "Zestawienie otrzymanych wyników";

				//Style table column names
				cellRang = excelWorkSheet.get_Range("A4", "Z4");
				cellRang.Font.Bold = true;
				cellRang.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
				cellRang.Interior.Color = System.Drawing.ColorTranslator.FromHtml("#3188ED");
				excelWorkSheet.get_Range("F4").EntireColumn.HorizontalAlignment = OfficeExcel.XlHAlign.xlHAlignRight;
				//Formate price column
				excelWorkSheet.get_Range("F5").EntireColumn.NumberFormat = "0.00";
				//Auto fit columns
				excelWorkSheet.Columns.AutoFit();
			}

			//Delete First Page
			excelApp.DisplayAlerts = false;
			Microsoft.Office.Interop.Excel.Worksheet lastWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)excelWorkBook.Worksheets[1];
			lastWorkSheet.Delete();
			excelApp.DisplayAlerts = true;

			//Set Defualt Page
			(excelWorkBook.Sheets[1] as OfficeExcel._Worksheet).Activate();

			excelWorkBook.SaveAs(strPath, Default, Default, Default, false, Default, OfficeExcel.XlSaveAsAccessMode.xlNoChange, Default, Default, Default, Default, Default);
			excelWorkBook.Close();
			excelApp.Quit();

			MessageBox.Show("Excel generated successfully \n As " + strPath);
		}
	}
}