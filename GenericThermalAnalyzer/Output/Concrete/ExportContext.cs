﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using GenericThermalAnalyzer.Input.Abstract;
using GenericThermalAnalyzer.Output.Abstract;
using JetBrains.Annotations;

namespace GenericThermalAnalyzer.Output.Concrete
{
	class ExportContext
	{
		private readonly Dictionary<string, ITableExport> _readers;

		public ExportContext() => _readers = new Dictionary<string, ITableExport>(10)
		{
			{"xlsx", new Export2Xls()}
		};

		[NotNull]
		[ItemNotNull]
		public IEnumerable<string> GetSupportedFormats => _readers.Keys;

		public void ExportTable(DataTable dataTable)
		{
			String filePath = "C:\\Users\\Przemek\\Desktop";
			var inputFile = new ExportData(filePath);
			ExportTables(inputFile);
		}

		public void ExportTables(ExportData exportData)
		{
			if (exportData == null)
				throw new ArgumentNullException(nameof(exportData),
												"Cannot read the specified document because it's null.");

			ITableExport reader = GetExportData("xlsx");

		}

		public ITableExport GetExportData([NotNull] string extension)
		{
			ITableExport exportDocument;
			return _readers.TryGetValue(extension, out exportDocument) ? exportDocument : null;
		}
	}
}