﻿using System;
using System.Data;
using JetBrains.Annotations;

namespace GenericThermalAnalyzer.Output.Abstract
{
	public interface ITableExport
	{
		void ExportTable(DataTable dataTable, String path, String filename);
	}
}