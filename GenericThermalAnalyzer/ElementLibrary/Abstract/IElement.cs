﻿using System.Collections.Generic;

namespace GenericThermalAnalyzer.ElementLibrary.Abstract
{
	/// <summary>
	///     Library element.
	///     Allows custom parameters to be set, instead of calling subclasses dedicated methods
	/// </summary>
	public interface IElement
	{
		/// <summary>
		///     Get list of parameters
		/// </summary>
		/// <returns>List of parameter names</returns>
		List<string> GetParameters();

		/// <summary>
		///     Get value of parameter
		/// </summary>
		/// <param name="param">Parameter name</param>
		/// <returns>Parameter value</returns>
		double GetParam(string param);

		/// <summary>
		///     Set parameter value
		/// </summary>
		/// <param name="param">Parameter name</param>
		/// <param name="value">Parameter value</param>
		void SetParam(string param, double value);
	}
}