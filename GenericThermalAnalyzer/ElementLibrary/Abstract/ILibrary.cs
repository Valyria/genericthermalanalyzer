﻿using System.Collections.Generic;

namespace GenericThermalAnalyzer.ElementLibrary.Abstract
{
	/// <summary>
	///     Library is meant to gather constructors of TElement subclasses
	///     on runtime and instantiate TElement subclasses on demand
	/// </summary>
	/// <typeparam name="TElement">Inherits IElement interface</typeparam>
	public interface ILibrary<out TElement> where TElement : IElement
	{
		/// <summary>
		///     Get list of available classes
		/// </summary>
		/// <returns>List of class names</returns>
		List<string> GetElementsList();

		/// <summary>
		///     Get instance of TElement subclass
		/// </summary>
		/// <param name="key">Subclass key</param>
		/// <returns>TElement subclass instance</returns>
		TElement GetElement(string key);
	}
}