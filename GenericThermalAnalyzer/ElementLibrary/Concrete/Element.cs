﻿using System;
using System.Collections.Generic;
using System.Linq;
using GenericThermalAnalyzer.ElementLibrary.Abstract;

namespace GenericThermalAnalyzer.ElementLibrary.Concrete
{
	/// <summary>
	///     Standard IElement implementation
	///     Allows its subclasses to add parameter checks
	/// </summary>
	public class Element : IElement
	{
		private readonly Dictionary<string, Func<double, bool>> _parametersConsraintDictionary;
		private readonly Dictionary<string, double> _parametersDictionary;

		public Element()
		{
			_parametersDictionary = new Dictionary<string, double>();
			_parametersConsraintDictionary = new Dictionary<string, Func<double, bool>>();
		}

		/// <inheritdoc />
		public List<string> GetParameters() => _parametersDictionary.Keys.ToList();

		/// <inheritdoc />
		public double GetParam(string param)
		{
			if (!_parametersDictionary.ContainsKey(param))
				return double.NaN;
			return _parametersDictionary[param];
		}

		/// <inheritdoc />
		public void SetParam(string param, double value)
		{
			if (_parametersConsraintDictionary.ContainsKey(param))
			{
				var cstr = _parametersConsraintDictionary[param];
				if (cstr != null)
					if (!cstr(value))
						throw new ArgumentException($"Invalid value for parameter {param}: {value}");
			}

			_parametersDictionary[param] = value;
		}

		protected void AddConstraint(string param, Func<double, bool> valueCheckFunc)
		{
			_parametersConsraintDictionary[param] = valueCheckFunc;
		}
	}
}