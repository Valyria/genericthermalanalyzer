﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using GenericThermalAnalyzer.DistributionFunctions.Abstract;
using GenericThermalAnalyzer.ElementLibrary.Abstract;

namespace GenericThermalAnalyzer.ElementLibrary.Concrete
{
	/// <summary>
	///     Standard implementation of ILibrary
	/// </summary>
	/// <typeparam name="TElement">Inherits IElement interface</typeparam>
	public class Library<TElement> : ILibrary<TElement> where TElement : IElement
	{
		private readonly Dictionary<string, ConstructorInfo> internal_dictionary;

		public Library()
		{
			internal_dictionary = new Dictionary<string, ConstructorInfo>();
			var type = typeof(IDistributionFunction);
			var types = AppDomain.CurrentDomain.GetAssemblies()
								 .SelectMany(s => s.GetTypes())
								 .Where(p => type.IsAssignableFrom(p) && p.IsClass && !p.IsAbstract);
			foreach (var t in types)
			{
				string name = Regex.Replace(t.Name, "([a-z])([A-Z])", "$1 $2");
				internal_dictionary.Add(name, t.GetConstructor(new Type[0]));
			}
		}

		public List<string> GetElementsList()
		{
			var list = internal_dictionary.Keys.ToList();
			list.Sort();
			return list;
		}

		public TElement GetElement(string key)
		{
			if (!internal_dictionary.ContainsKey(key))
				return default(TElement);
			var constructor = internal_dictionary[key];
			var value = (TElement) constructor.Invoke(null);
			return value;
		}
	}
}