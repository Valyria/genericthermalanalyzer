﻿using System.Data;
using GenericThermalAnalyzer.DistributionFunctions.Abstract;

namespace GenericThermalAnalyzer.Transition.Abstract
{
	/// <summary>
	///     Transition interface
	/// </summary>
	public interface ITransition
	{
		/// <summary>
		///     Temperature at which transition begins
		/// </summary>
		double InitialTemperature { get; set; }

		/// <summary>
		///     Temperature at which transition ends
		/// </summary>
		double FinalTemperature { get; set; }

		/// <summary>
		///     Total enthalpy of transition
		/// </summary>
		double TotalEnthalpy { get; set; }

		/// <summary>
		///     Distribution function
		/// </summary>
		IDistributionFunction DistributionFunction { get; set; }

		/// <summary>
		///     Transition name f.e. "Melting"
		/// </summary>
		string Name { get; set; }

		/// <summary>
		///     For each row in DataTable computes accumulated enthalpy according to chosen distribution function
		///     Adds column "{Name}Enthalpy" to DataTable passed as parameter
		/// </summary>
		/// <param name="dataTable">Input DataTable, "Temperature" Column of type double is required</param>
		/// <param name="overridePrevious">
		///     Override existing column if true, add
		///     new column with different suffix otherwise
		/// </param>
		void ComputeEnthalpyTable(DataTable dataTable, bool overridePrevious = true);
	}
}