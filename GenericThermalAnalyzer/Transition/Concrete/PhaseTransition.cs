﻿using System;
using System.Data;
using GenericThermalAnalyzer.DistributionFunctions.Abstract;
using GenericThermalAnalyzer.DistributionFunctions.Concrete;
using GenericThermalAnalyzer.Transition.Abstract;

namespace GenericThermalAnalyzer.Transition.Concrete
{
	/// <summary>
	///     Standard implementation of ITransition
	/// </summary>
	public class PhaseTransition : ITransition
	{
		private double _FinalTemperature = 1000;
		private double _InitialTemperature = 500;
		private double _TotalEnthalpy = 100;

		/// <inheritdoc />
		public double InitialTemperature
		{
			get => _InitialTemperature;
			set
			{
				if (value > _FinalTemperature)
					throw new Exception("Initial temperature must not be higher than final.");
				if (value < 0.0)
					throw new Exception("Negative temperatures are not accepted.");
				_InitialTemperature = value;
			}
		}

		/// <inheritdoc />
		public double FinalTemperature
		{
			get => _FinalTemperature;
			set
			{
				if (value <= _InitialTemperature)
					throw new Exception("Final temperature must not be lower than initial.");
				if (value < 0.0)
					throw new Exception("Negative temperatures are not accepted.");
				_FinalTemperature = value;
			}
		}

		/// <inheritdoc />
		public double TotalEnthalpy
		{
			get => _TotalEnthalpy;
			set
			{
				if (value <= 0.0) throw new Exception("Enthalpy must be greater than zero.");
				_TotalEnthalpy = value;
			}
		}

		/// <inheritdoc />
		public IDistributionFunction DistributionFunction { get; set; } = new FlatDistributionFunction();

		/// <inheritdoc />
		public string Name { get; set; }

		/// <inheritdoc />
		public void ComputeEnthalpyTable(DataTable dataTable, bool overridePrevious = true)
		{
			if (!dataTable.Columns.Contains("Temperature"))
				throw new Exception("Missing Temperature column.");
			var temperature = dataTable.Columns["Temperature"];
			if (temperature.DataType != typeof(double))
				throw new Exception($"Invalid Temperature column type: {temperature.DataType.Name}, expected double");
			var enthalpy = new DataColumn($"{Name}Enthalpy", typeof(double));
			if (dataTable.Columns.Contains(enthalpy.ColumnName))
			{
				if (overridePrevious)
				{
					dataTable.Columns.Remove(enthalpy.ColumnName);
				}
				else
				{
					int count = 0;
					string name;
					do
					{
						count++;
						name = $"{enthalpy.ColumnName}#{count}";
					} while (dataTable.Columns.Contains(name));

					enthalpy.ColumnName = name;
				}
			}

			dataTable.Columns.Add(enthalpy);
			foreach (DataRow row in dataTable.Rows)
			{
				double normalized;
				if (Math.Abs(FinalTemperature - InitialTemperature) > 1e-3)
					normalized = ((double) row[temperature] - InitialTemperature) / (FinalTemperature - InitialTemperature);
				else
					normalized = (double) row[temperature] >= InitialTemperature ? 1.0 : 0.0;

				double transitionProgress = DistributionFunction.Call(normalized);
				row[enthalpy] = transitionProgress * TotalEnthalpy;
			}
		}
	}
}