﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using GenericThermalAnalyzer.DistributionFunctions.Abstract;
using GenericThermalAnalyzer.ElementLibrary.Abstract;
using GenericThermalAnalyzer.ElementLibrary.Concrete;
using GenericThermalAnalyzer.Gui;
using GenericThermalAnalyzer.Transition.Abstract;
using GenericThermalAnalyzer.Transition.Concrete;
using GenericThermalAnalyzer.Utility;

namespace GenericThermalAnalyzer
{
    internal static class Program
    {
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

            Application.ThreadException += (sender, args) => MessageBox.Show(
                $"An unhandled exception has occurred within the UI thread: {args.Exception.Message}\n\n" +
                $"{args.Exception.StackTrace}\n\nThe application may still be used but it's recommended to restart it in order to avoid any potential issues.",
                "UI Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            AppDomain.CurrentDomain.UnhandledException += (sender, args) => MessageBox.Show(
                $"An uncaught exception has been thrown within the current application domain:\n\n{args.ExceptionObject.ToString()}",
                "Uncaught Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);

            try
            {
                Application.Run(new MainWindow());
            }
            catch (Exception e)
            {
                MessageBox.Show(
                    $"The program has encountered a fatal unresolved error and will now shut down: {e.Message}\n\n{e.StackTrace}",
                    "Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Run this example to see how ITransition computes enthalpies for all data rows
        /// </summary>
        private static void Example()
        {
            // Create new Library<T> object. It will load all IDistributionFunction subclasses automatically.
            ILibrary<IDistributionFunction> library = new Library<IDistributionFunction>();
            // Get names of all available functions
            List<String> elementsList = library.GetElementsList();
            Console.WriteLine(String.Join(", ", elementsList.ToArray()));
            // Create example DataTable
            DataTable table = new DataTable("Stuff");
            // ... with Temperature column of type double (required)
            table.Columns.Add("Temperature", typeof(double));
            for (double temperature = 10.0; temperature < 1000.0; temperature += 10.0)
                table.Rows.Add(temperature);
            // Define PhaseTransition object
            ITransition transition = new PhaseTransition();
            // Select distribution function - get it by name from Library
            transition.DistributionFunction = library.GetElement("Normal Distribution Function");
            // Set FinalTemperature first - to avoid errors on validity check
            transition.FinalTemperature = 900.0;
            transition.InitialTemperature = 700.0;
            // Name transition
            transition.Name = "Melting";
            // Set total Enthalpy of transition
            transition.TotalEnthalpy = 100.0;
            // Compute enthalpy for each temperature - run multiple times to see combining feature
            for (int i = 0; i < 5; i++)
                transition.ComputeEnthalpyTable(table, overridePrevious: false);
            // Combine all enthalpies without dropping columns
            EnthalpyCombiner.CombineEnthalpies(table, drop: false);
            // Print DataTable columns
            foreach (DataColumn tableColumn in table.Columns)
            {
                Console.WriteLine($"{tableColumn.ColumnName}:{tableColumn.DataType.Name}");
            }
            // Print result
            foreach (DataRow row in table.Rows)
            {
                Console.WriteLine($"{row["Temperature"]}: {row["TotalEnthalpy"]}");
            }
        }
    }
}